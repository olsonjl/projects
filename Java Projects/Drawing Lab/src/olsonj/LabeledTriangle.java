/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */
package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.scene.paint.Color;

/**
 *the LabeledTriangle class extends triangle
 */
public class LabeledTriangle extends Triangle {
    private String name;


    /**
     * the labeledtriangle constructor calls the parent variables and any other
     * variables need to construct a triangle
     * @param x     x coordinate
     * @param y     y coordinate
     * @param base  base length
     * @param height    height length
     * @param color     color
     * @param name      label name
     */
    public LabeledTriangle(double x, double y, double base, double height, Color color, String name){
        super(x, y, base, height, color);
        try {
            if(base > 0 && height > 0) {
                this.name = name;
            } else {
                throw new IllegalArgumentException("The base and/or height is below 0");
            }
        } catch (IllegalArgumentException e){
            System.out.println("The base and/or height is below 0");
        }

    }

    /**
     * allows the driver to draw a labeled triangle
     * @param plotter reference to a WinPlotter which is sent in
     */
    public void draw(WinPlotterFX plotter){
        super.draw(plotter);
        plotter.printAt(x + base/4 +4, y + height/3 +3, name);
    }
}
