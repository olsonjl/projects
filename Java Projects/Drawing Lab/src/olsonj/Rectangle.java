/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */
package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.scene.paint.Color;

/**
 * Description: Rectangle class a width, a height, and points sent to create a rectangle on the grid
 *
 * @author Jace Olson
 * @verison 2018.12.18
 */
public class Rectangle extends Shape {
    protected double height;
    protected double width;

    /**
     * The rectangle contructor
     * @param x     passes in the x coordinate
     * @param y     passes in the y coordinate
     * @param width passe in the width
     * @param height passes in the height
     * @param color passes in the color
     */
    public Rectangle(double x, double y, double width, double height, Color color){
        super(x, y, color);
        try {
            if(width > 0 && height > 0) {
                this.width = width;
                this.height = height;
            } else {
                throw new IllegalArgumentException("The width and height aren't above 0");
            }
        }catch(IllegalArgumentException e){
            System.out.println("The width and height aren't above 0;");
        }

    }

    /**
     * The draw method allows the driver to draw the rectangle
     * @param plotter reference to a WinPlotter which is sent in
     */
    public void draw(WinPlotterFX plotter){
        setPenColor(plotter);
        plotter.moveTo(x,y);
        plotter.drawTo(x + width, y);
        plotter.drawTo(x + width, y + height);
        plotter.drawTo(x, y + height);
        plotter.drawTo(x, y);
    }
}
