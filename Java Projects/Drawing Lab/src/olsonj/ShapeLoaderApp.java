/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */

package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.IOException;


import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;



public class ShapeLoaderApp extends Application {
    public static Scanner in;
    WinPlotterFX plotter = new WinPlotterFX();
    FileChooser fileChooser = new FileChooser();
    ArrayList<Shape> shapes = new ArrayList<>();

    /**
     * The start method controls the
     * @param stage
     */
    public void start(Stage stage) {
        try {
            fileChooser.setTitle("File Chooser");
            in = new Scanner(fileChooser.showOpenDialog(stage));

            plotter.setWindowTitle(in.nextLine());
            plotter.setWindowSize(in.nextInt(), in.nextInt());
            in.nextLine();
            readShapes();
            drawShapes();
            plotter.showPlotter();;

        } catch (IOException e) {
            Alert ioAlert = new Alert(Alert.AlertType.ERROR);
            ioAlert.setTitle("Error Dialog");
            ioAlert.setHeaderText("Program would not run");
            ioAlert.setContentText("There was an IOException");

            ioAlert.showAndWait();
        } catch (InputMismatchException e) {
            Alert inputAlert = new Alert(Alert.AlertType.ERROR);
            inputAlert.setTitle("Error Dialog");
            inputAlert.setHeaderText("Program would not run");
            inputAlert.setContentText("There was an InputMismatchException");

            inputAlert.showAndWait();
        } finally {
            in.close();
        }
    }


    /**
     * draws the shapes using the plotter
     */
    public void drawShapes() {
        for(int i = 0; i < shapes.size(); i++){
            Shape drawShape = shapes.get(i);
            drawShape.draw(plotter);
        }

    }

    /**
     * reads each shape and puts in into an ArrayList
     */
    public void readShapes() {
        Shape newShape;
        shapes = new ArrayList<>();

        while(in.hasNextLine()){
            try{
                newShape = parseShape(in.nextLine());
                shapes.add(newShape);
            } catch(NoSuchElementException e){
                System.out.println("There was a no element exception");
            }
        }
    }

    /**
     * Converts the color into a string so it can be read from the Shape class and subclasses
     * @param colorCode hex triplet
     * @return color
     * @throws InputMismatchException when the color is not in hex triplet form
     */
    public static Color stringToColor(String colorCode) throws InputMismatchException {
        Color color = null;
        try {
            if (colorCode.length() == 7) {
                color = Color.web(colorCode);
            } else {
                throw new InputMismatchException("The color was not a hex triplet code");
            }
        } catch (InputMismatchException e) {
            Alert inputAlert = new Alert(Alert.AlertType.ERROR);
            inputAlert.setTitle("Error Dialog");
            inputAlert.setHeaderText("There is an input mismatch exception");
            inputAlert.setContentText("Cannot run program; The color was not a hex triplet code");

            inputAlert.showAndWait();
        }

        return color;


    }

    /**
     * parses each shape and allows them to be read and added into the ArrayList in the readShapes method
     * @param line the next line in the file
     * @return the Shape that is needed
     */
    public Shape parseShape(String line){
        Shape newShape = new Point(10, 10, Color.color(1,1,1));
        Scanner shapeReader  = new Scanner(line);
        Color color;
        int xCoord;
        int yCoord;
        int height;
        int width;
        int base;
        int radius;
        String name;
        String shapeText = shapeReader.next();
        if(shapeText.equals("P:")){

            xCoord = shapeReader.nextInt();
            yCoord = shapeReader.nextInt();
            color = stringToColor(shapeReader.next());
            newShape = new Point(xCoord, yCoord, color);

        } else if (shapeText.equals("C:")){
            xCoord = shapeReader.nextInt();
            yCoord = shapeReader.nextInt();
            color = stringToColor(shapeReader.next());
            radius = shapeReader.nextInt();
            newShape = new Circle(xCoord, yCoord, radius, color);

        } else if(shapeText.equals("T:")){
            xCoord = shapeReader.nextInt();
            yCoord = shapeReader.nextInt();
            color = stringToColor(shapeReader.next());
            base = shapeReader.nextInt();
            height = shapeReader.nextInt();
            newShape = new Triangle(xCoord, yCoord, base, height, color);

        } else if (shapeText.equals("R:")){
            xCoord = shapeReader.nextInt();
            yCoord = shapeReader.nextInt();
            color = stringToColor(shapeReader.next());
            width = shapeReader.nextInt();
            height = shapeReader.nextInt();
            newShape = new Rectangle(xCoord, yCoord, width, height, color);

        } else if (shapeText.equals("LT:")){

            xCoord = shapeReader.nextInt();
            yCoord = shapeReader.nextInt();
            color = stringToColor(shapeReader.next());
            base = shapeReader.nextInt();
            height = shapeReader.nextInt();
            name = shapeReader.nextLine();
            newShape = new LabeledTriangle(xCoord, yCoord, base, height, color, name);

        } else if (shapeText.equals("LR:")){
            xCoord = shapeReader.nextInt();
            yCoord = shapeReader.nextInt();
            color = stringToColor(shapeReader.next());
            width = shapeReader.nextInt();
            height = shapeReader.nextInt();
            name = shapeReader.nextLine();
            newShape = new LabeledRectangle(xCoord, yCoord, width, height, color, name);
        }


        return newShape;
    }

    /**
     * launches arguments
     * @param args arguments in the class
     */
    public static void main(String[] args) {
        launch(args);
    }

}