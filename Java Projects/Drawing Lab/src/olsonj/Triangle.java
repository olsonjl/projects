/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */
package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.scene.paint.Color;

/**
 * The Triangle class extends shape
 */
public class Triangle extends Shape {
    protected double base;
    protected double height;

    /**
     * the triangle constructor calls the parent variables and any other
     * variables need to construct a triangle
     * @param x     x coordinate
     * @param y     y coordinate
     * @param base  base length
     * @param height    height length
     * @param color     color
     */
    public Triangle(double x, double y, double base, double height, Color color){
        super(x, y, color);
        try {
            if(base > 0 && height > 0) {
                this.base = base;
                this.height = height;
            } else {
                throw new IllegalArgumentException("The base and height were below zero");
            }
        } catch(IllegalArgumentException e) {
            System.out.println("The base and height were below zero");
        }

    }

    /**
     * allows the driver to draw a triangle
     * @param plotter reference to a WinPlotter which is sent in
     */
    public void draw(WinPlotterFX plotter){
        setPenColor(plotter);
        plotter.moveTo(x , y);
        plotter.drawTo(x + base, y);
        plotter.drawTo(x + base/2, y + height);
        plotter.drawTo(x, y);

    }

}
