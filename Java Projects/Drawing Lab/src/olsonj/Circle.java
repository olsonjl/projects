/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */
package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.scene.paint.Color;

/**
 * The Circle class allows the use of a circle in the driver
 */
public class Circle extends Shape {


    private final double radius;

    /**
     * The Circle constructor is passed in everything it needs to construct a circle
     * @param x     passes in the x coordinate
     * @param y     passes in the y coordinate
     * @param radius   passes in the radius
     * @param color     passes in the color
     */
    public Circle(double x, double y, double radius, Color color){
        super(x, y, color);
        this.radius = radius;
    }

    /**
     * allows the driver to draw a circle
     * @param plotter reference to a WinPlotter which is sent in
     */
    public void draw(WinPlotterFX plotter){
        setPenColor(plotter);
        plotter.moveTo(x+radius,y);
        for(int i = 0; i < 360; i++) {
            plotter.drawTo((x+(radius*((Math.cos(Math.toRadians(i)))))), (y+(radius*((Math.sin(Math.toRadians(i)))))));
        }
    }
}
