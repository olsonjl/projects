/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */
package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.scene.paint.Color;

/**
 * Description: Point class takes points sent and creates dots on the grid
 *
 * @author Jace Olson
 * @version 2018.12.18
 */
public class Point extends Shape {

    /**
     * Point constructor takes in values to save in super
     * @param x x coordinate of point
     * @param y y coordinate of point
     * @param color color of point
     */
    public Point (double x, double y, Color color){

        super(x, y, color);
    }

    /**
     * draw method sets pen color, draw point, and starts drawing
     * @param plotter reference to a WinPlotter which is sent in
     */
    public void draw(WinPlotterFX plotter){
        setPenColor(plotter);
        plotter.moveTo(x, y);
        plotter.drawPoint(x - 5, y -5);
    }

}
