/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */
package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.scene.paint.Color;

/**
 * Description: Shape class the super, defining all shapes and saving all generic variables
 *
 * @author olsonj
 * @version 2018.12.18
 */
public abstract class Shape {
    private Color color;
    protected double x;
    protected double y;

    /**
     * Shape constructor represents generic shape parameters as x coordinate, y coordinate, and color
     * @param x x coordinate of the starting corner of the shape
     * @param y y coordinate of the starting corner of the shape
     * @param color color of the shape, as seen on the grid
     */
    public Shape(double x, double y, Color color) {
        this.color = color;
        try {
            if(x > 0 && y > 0) {
                this.x = x;
                this.y = y;

            } else {
                throw new IllegalArgumentException("Numbers entered were not above 0");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException found");
        }
    }
    /**
     * draw method is abstract sending responsibilities to subclasses
     * @param plotter reference to a WinPlotter which is sent in
     */
    public abstract void draw(WinPlotterFX plotter);

    /**
     * setPenColor sets drawing color for shape
     *
     * @param plotter reference to a WinPlotter which is sent in
     */
    public void setPenColor(WinPlotterFX plotter){
        double red = color.getRed();
        double green = color.getGreen();
        double blue = color.getBlue();
        plotter.setPenColor(red, green, blue);
    }

    /**
     * setColor sets color of shape sent in
     * @param color color sent in to eventually be used in setPenColor
     */
    public void setColor(Color color){
        this.color = color;
    }
}
