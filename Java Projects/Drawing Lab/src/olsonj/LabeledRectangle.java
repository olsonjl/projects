/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 7- Shapes Revisited
 * Name: Jace Olson
 * Created: 1/24/19
 */
package olsonj;

import edu.msoe.winplotterfx.WinPlotterFX;
import javafx.scene.paint.Color;

/**
 * the labeledrectangle class allows the driver to label a rectanlge
 */
public class LabeledRectangle extends Rectangle {

    private String name;

    /**
     * the LabeledRectangle contructor takes in all of the needed parameters
     * @param x         passes in the x coordinate
     * @param y         passes in the y coordinate
     * @param width     passes in the width
     * @param height    passes in the height
     * @param color     passes in the color
     * @param name      passes in the name
     */
    public LabeledRectangle(double x, double y, double width, double height, Color color, String name){
        super(x, y, width, height, color);
        try {
            if(width > 0 && height > 0) {
                this.name = name;
            } else {
                throw new IllegalArgumentException("The width and/or height were below 0");
            }
        } catch (IllegalArgumentException e){
            System.out.println("The width and/or height were below 0");
        }

    }

                    /**
                     * allows the driver to draw a rectangle that has a label
                     * @param plotter reference to a WinPlotter which is sent in
                     */
            public void draw(WinPlotterFX plotter){
                super.draw(plotter);
                plotter.printAt(x + width/150 - 12, y + height/2, name);
    }
}
