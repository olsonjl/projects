/*
 * CS1021 - 051
 * Winter 2018-2019
 * Lab Game Of Life
 * Name: Jace Olson
 * Created 1/10/2019
 */
package olsonj;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;




public class Lab5 extends Application {

    private Label deadCell;
    private Label aliveCell;
    private LifeGrid lifeGrid;

    private Pane gamePane;

    /**
     * sets the scene, title and shows the stage
     * @param primaryStage it's the main stage
     */
    public void start(Stage primaryStage){
        gamePane = new Pane();

        gamePane.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                cellCount();
            }
        });

        createContents(gamePane);

        Scene scene = new Scene(gamePane, 400, 500);
        primaryStage.setTitle("The Game of Life");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();


    }

    /**
     * creation of the buttons and labels and the hboxes that hold them
     * @param pane holds all of the contents
     */
    private void createContents(Pane pane){
        lifeGrid = new LifeGrid(pane, 400, 400 );

        Button resetButton = new Button();
        resetButton.setText("Reset");
        resetButton.setOnAction(this::resetRespond);
        pane.getChildren().add(resetButton);
        resetButton.setAlignment(Pos.BOTTOM_RIGHT);


        Button stepButton = new Button();
        stepButton.setText("Step");
        stepButton.setOnAction(this::stepRespond);
        pane.getChildren().add(stepButton);
        stepButton.setAlignment(Pos.BOTTOM_LEFT);

        HBox hbox = new HBox();
        hbox.setSpacing(10);
        hbox.setPadding(new Insets(5));
        hbox.setLayoutX(150);
        hbox.setLayoutY(445);
        hbox.getChildren().addAll(stepButton,resetButton);



        aliveCell = new Label();
        deadCell = new Label();

        HBox labels = new HBox();
        labels.setSpacing(10);
        labels.setPadding(new Insets(5));
        labels.setLayoutX(150);
        labels.setLayoutY(475);
        labels.getChildren().addAll(aliveCell, deadCell);

        pane.getChildren().addAll(hbox, labels);

        lifeGrid.randomize();
        cellCount();
    }

    /**
     * resets the game when the button is presse
     * @param e does nothing
     */
    private void resetRespond(ActionEvent e){
        lifeGrid.randomize();
        cellCount();
    }

    /**
     * this allows the player to proceed to the next step of the game
     * @param e does nothing
     */
    private void stepRespond(ActionEvent e){
        lifeGrid.iterate();
        cellCount();
    }

    /**
     *This method counts the number of alive and dead cells on the grid
     */
    public void cellCount(){
        aliveCell.setText("Alive: " + Integer.toString(lifeGrid.countAliveCells()));
        deadCell.setText("Dead: " + Integer.toString(lifeGrid.countDeadCells()));
    }


 }

