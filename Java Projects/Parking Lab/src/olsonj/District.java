/*
 * CS- 1011 81
 * Fall 2018
 * Lab 8 - Districts
 * Name: Jace Olson
 * Created: 10/31/2017
 */

package olsonj;

import java.util.ArrayList;

/**
 * @author Jace Olson
 * <p/>
 * Capture usage information for parking lots in a district.
 */
public class District {
    public static final int MAX_LOTS = 20;

    private ArrayList<ParkingLot> lots = new ArrayList<>();
    private int lastEntryTime;
    private int lastExitTime;
    private int closedMinutes;
    private int numLots;

    /**
     * The addLot method adds a new lot
     * @param name is the name of the new lot
     * @param capacity is the capacity of the new lot
     * @return the int of the new index of the parking that was added
     */
    public int addLot(String name, int capacity) {
        int newIndex = numLots;
        if (newIndex < MAX_LOTS) {
            lots.add(new ParkingLot(name, capacity));
            numLots++;
            //return the index of the new lot or -1 if the lot was not added
        }
        return newIndex < MAX_LOTS ? newIndex : -1;

    }

    /**
     * the getLot method gets the lot at the certain index
     * @param index is the index of the new lot
     * @return the parking lot with that index
     */
    public ParkingLot getLot(int index) {
        return lots.get(index);
    }

    /**
     * the toString() converts the status into a string
     * @return a string that contains the status
     */
    public String toString() {
        String string = "District status:\n";
        for (int i = 0; i <= numLots - 1; i++) {
            string += lots.get(i).toString();
            string += "\n";
        }
        return string;
    }


    /**
     * Record a vehicle entering a given lot at a given time.
     *
     * @param lotNumber Number of lot, 1-3
     * @param time      Entry time in minutes since all lots were opened.
     *                  This calls ParkingLot.markVehicleEntry for the lot corresponding
     *                  to lotNumber (1 -> lot1, 2 -> lot2, 3 -> lot3).
     *                  If lotNumber is out of range, the behavior is unspecified.
     */
    public void markVehicleEntry(int lotNumber, int time) {
        if (isClosed()) {
            if (lastEntryTime > time) {
                closedMinutes = closedMinutes + (time - lastEntryTime);
            } else {
                closedMinutes = closedMinutes + (time - lastExitTime);
            }
        }
        getLot(lotNumber).markVehicleEntry(time);
        lastEntryTime = time;
    }

    /**
     * Record a vehicle exiting a given lot at a given time.
     *
     * @param lotNumber Number of lot, 1-3
     * @param time      Entry time in minutes since all lots were opened.
     *                  This calls ParkingLot.markVehicleExit for the lot corresponding
     *                  to lotNumber (1 -> lot1, 2 -> lot2, 3 -> lot3).
     *                  If lotNumber is out of range, the behavior is unspecified.
     */
    public void markVehicleExit(int lotNumber, int time) {
        if (isClosed()) {
            if (lastExitTime > lastEntryTime) {
                closedMinutes += time - lastExitTime;
            } else {
                closedMinutes += time - lastEntryTime;
            }
        }
        getLot(lotNumber).markVehicleExit(time);
        lastExitTime = time;
    }

    /**
     * Checks the status of all three lots in the district and
     * returns true if they are all closed and false otherwise.
     *
     * @return whether all lots are closed in the district
     */
    public boolean isClosed() {
        boolean closed = true;
        int i = 0;
        while (i < numLots) {
            closed = closed && lots.get(i).isClosed();
            i++;
        }
        return closed;
    }

    /**
     * the vehiclesParkedInDistrict() method counts the number of
     * vehicles parked in a dsitrict
     * @return the amount of vehicles parked in the district
     */
    public int vehiclesParkedInDistrict() {
        int sum = 0;
        for (int i = 0; i < numLots; i++) {
            sum += lots.get(i).vehiclesInLot();
        }
        return sum;
    }

    /**
     * Computes the time all lots were reported as closed.
     *
     * @return number of minutes all three lots were closed
     */
    public int closedMinutes() {
        return closedMinutes;
    }
}

