/*
 * CS- 1011 81
 * Fall 2018
 * Lab 8 - ParkingLots
 * Name: Jace Olson
 * Created: 10/31/2017
 */

package olsonj;


import java.text.DecimalFormat;

/**
 * the ParkingLot class lets there be different color parking lots with
 * different capacities
 * the parking lots close when they hit 80% capacity
 */
public class ParkingLot {

    private String name;
    public static final double CLOSED_THRESHOLD = 80.0;
    private int capacity;
    private int currentCapacity;
    private int lastExitTime;
    private int closedMinutes;
    private int lastEntryTime;
    String pattern = "##.#";
    DecimalFormat decimalFormat = new DecimalFormat(pattern);

    /**
     * the parking lot constructor takes in the name and capacity of the parking lot
     *
     * @param lotName  is the name of the given parking lot
     * @param capacity is the capacity of the parking lot
     */
    public ParkingLot(String lotName, int capacity) {
        this.name = lotName;
        this.capacity = capacity;
    }

    /**
     * ParkingLot is a construtor that only takes in the capacity.
     * the default color for this parking lot is "test"
     *
     * @param capacity is the capcity of the parking lot
     */
    public ParkingLot(int capacity) {
        this.capacity = capacity;
        this.name = "test";
    }

    /**
     * the get name method gets the name of a certain parking lot
     *
     * @return the name of the parking lot
     */
    public String getName() {
        return name;
    }

    /**
     * the markVehicleEntry method marks the vehicles that enter the parking lot
     *
     * @param time represents the time when a vehicle enters
     */
    public void markVehicleEntry(int time) {
        if (lastEntryTime <= time) {
            if (isClosed()) {
                closedMinutes = closedMinutes + (time - lastEntryTime);
                lastEntryTime = time;
            } else {
                lastEntryTime = time;
            }
            currentCapacity++;
        }

    }

    /**
     * the markVehicleExit method marks when a vehicle exits the parking lot
     *
     * @param time represents time
     */
    public void markVehicleExit(int time) {
        if (lastExitTime <= time && time >= lastEntryTime) {
            if (isClosed()) {
                if (lastExitTime > lastEntryTime) {
                    closedMinutes = closedMinutes + (time - lastExitTime);
                    lastExitTime = time;
                } else {
                    lastExitTime = time;
                    closedMinutes = closedMinutes + (lastExitTime - lastEntryTime);
                }
            } else {
                lastExitTime = time;
            }
            currentCapacity--;
        }
    }

    /**
     * the vehiclesInLot() method tells the parking lot how many
     * cars are in the lot
     *
     * @return currentCapacity of the parking lot
     */
    public int vehiclesInLot() {
        return this.currentCapacity;
    }

    /**
     * the isClosed() method tells if the parking lot is closed
     *
     * @return true if closed and false if not
     */
    public boolean isClosed() {
        boolean closed = false;
        double percentage = (0.01 * CLOSED_THRESHOLD) * (double) capacity;
        if (percentage <= currentCapacity) {
            closed = true;
        }
        return closed;
    }

    /**
     * the closedMinutes() method tells how many minutes a parking lot was closed.
     *
     * @return closedMinutes
     */
    public int closedMinutes() {
        return this.closedMinutes;
    }

    /**
     * The toString() method puts the status of a lot into
     * a string
     */
    public String toString() {
        String status;
        if (isClosed()) {
            status = "Status for " + this.name + " parking lot: " + currentCapacity +
                    " vehicles (CLOSED)";
        } else {
            status = "Status for " + this.name + " parking lot: " + currentCapacity + " vehicles ("
                    + decimalFormat.format(((double) currentCapacity / capacity) * 100.0) + "%)";

        }
        return status;
    }
}
