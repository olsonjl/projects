/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 9 - Final Project
 * Name: Jace Olson
 * Created: 2/8/19
 */

package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Image manipulator");
        primaryStage.setScene(new Scene(root, 470, 310));

        FXMLLoader kernel = new FXMLLoader(getClass().getResource("window2.fxml"));
        Parent root2 = kernel.load();
        Stage stage2 = new Stage();
        stage2.setScene(new Scene(root2, 460, 248));

        Controller controller = loader.getController();
        FilterKernel kernelController = kernel.getController();
        kernelController.setFilterController(controller);
        controller.setKernelStage(stage2);

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
