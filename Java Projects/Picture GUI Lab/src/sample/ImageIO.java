/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 9 - Final Project
 * Name: Jace Olson
 * Created: 2/8/19
 */

package sample;

import javafx.scene.control.Alert;
import javafx.scene.image.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

import edu.msoe.cs1021.ImageUtil;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.Scanner;

import static edu.msoe.cs1021.ImageUtil.readImage;
import static edu.msoe.cs1021.ImageUtil.writeImage;

public class ImageIO {



    public Image read(Path path) {
        Image image = null;
        String string = path.toString();

        if (string.toLowerCase().endsWith(".msoe")) {
            image = readMSOE(path);
        } else if(string.toLowerCase().endsWith(".bmsoe")) {
            image = readBMSOE(path);
        } else {
            try {

                image = readImage(path);

            } catch (IOException e) {
                Alert ioAlert = new Alert(Alert.AlertType.ERROR);
                ioAlert.setTitle("Error");
                ioAlert.setContentText("There was an error");
                ioAlert.setHeaderText("Error");

                ioAlert.showAndWait();
            }
        }
        return image;
    }

    public static void write(Image image, Path path) {

        if (path.toString().endsWith(".msoe")) {

            writeMSOE(image, path);
        } else if (path.toString().endsWith(".bmsoe")) {
            writeBMSOE(image, path);
        } else{
            try {
                writeImage(path, image);

            } catch (IOException e) {
                Alert ioAlert = new Alert(Alert.AlertType.ERROR);
                ioAlert.setTitle("Error");
                ioAlert.setContentText("There was an error");
                ioAlert.setHeaderText("Error");

                ioAlert.showAndWait();
            }


        }
    }

    public Image readMSOE(Path path) throws IllegalArgumentException {

        WritableImage outImage = null;

        try {

            Scanner in = new Scanner(path);
            in.nextLine();
            outImage = new WritableImage(in.nextInt(), in.nextInt());

            in.nextLine();


            PixelWriter writer = outImage.getPixelWriter();

            for (int y = 0; y < outImage.getHeight(); y++) {
                for (int x = 0; x < outImage.getWidth(); x++) {

                    Color msoeColor = Color.web(in.next());
                    writer.setColor(x, y, msoeColor);
                }

            }
        } catch (IOException e) {
            Alert ioAlert = new Alert(Alert.AlertType.ERROR);
            ioAlert.setTitle("Error");
            ioAlert.setContentText("There was an error");
            ioAlert.setHeaderText("Error");

            ioAlert.showAndWait();
        }
        return outImage;
    }

    public static void writeMSOE(Image image, Path path) throws IllegalArgumentException {
        PixelReader reader = image.getPixelReader();
        PrintWriter writer;
        try {
            writer = new PrintWriter(path.toFile());
            writer.println("MSOE");
            writer.print((int) image.getWidth() + " " + (int) image.getHeight());
            writer.println();

            for (int y = 0; y < image.getHeight(); y++) {
                for (int x = 0; x < image.getWidth(); x++) {
                    Color color = reader.getColor(x, y);
                    writer.print(toRBGCode(color) + " ");

                }
                writer.println();

            }

            writer.close();
        } catch (IOException e) {
            Alert ioAlert = new Alert(Alert.AlertType.ERROR);
            ioAlert.setTitle("Error");
            ioAlert.setContentText("There was an error");
            ioAlert.setHeaderText("Error");

            ioAlert.showAndWait();
        }
    }

    public static String toRBGCode(Color color) {
        return String.format("#%02x%02x%02x",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
    }


    public static Image readBMSOE(Path path) {
        DataInputStream dataInputStream = null;
        WritableImage imageOut = null;
        PixelWriter pixelWriter;
        try {
            InputStream stream = Files.newInputStream(path);
            dataInputStream = new DataInputStream(stream);

            char B = (char) dataInputStream.readByte();
            char M = (char) dataInputStream.readByte();
            char S = (char) dataInputStream.readByte();
            char O = (char) dataInputStream.readByte();
            char E = (char) dataInputStream.readByte();

            int width = dataInputStream.readInt();
            int height = dataInputStream.readInt();

            imageOut = new WritableImage(width, height);
            pixelWriter = imageOut.getPixelWriter();

            for(int y = 0; y < height; y++){
                for(int x = 0; x < width; x++){
                    int color = dataInputStream.readInt();
                    pixelWriter.setColor(x, y, intToColor(color));
                }
            }

        } catch (IOException e) {
            Alert ioException = new Alert(Alert.AlertType.ERROR);
            ioException.setTitle("IOException");
            ioException.setContentText("Could not open stream");
            ioException.showAndWait();
        } finally {
            if(dataInputStream != null){
                try{
                    dataInputStream.close();
                } catch (IOException e) {
                    Alert ioException = new Alert(Alert.AlertType.ERROR);
                    ioException.setTitle("IOException");
                    ioException.setContentText("Could not close stream");
                    ioException.showAndWait();
                }
            }
        }
        return imageOut;

    }


    public static void writeBMSOE(Image image, Path path) {
        PixelReader pixelReader = image.getPixelReader();
        DataOutputStream dataOutputStream = null;
        try{
            OutputStream outputStream = Files.newOutputStream(path);
            dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.writeBytes("b");
            dataOutputStream.writeBytes("m");
            dataOutputStream.writeBytes("s");
            dataOutputStream.writeBytes("o");
            dataOutputStream.writeBytes("e");
            dataOutputStream.writeInt((int) image.getWidth());
            dataOutputStream.writeInt((int) image.getHeight());

            for(int x = 0; x < image.getWidth(); x++){
                for(int y = 0; y < image.getHeight(); y++){
                    Color imageColor = pixelReader.getColor(x,y);
                    int color = colorToInt(imageColor);
                    dataOutputStream.writeInt(color);
                }
            }

        } catch (IOException e){
            Alert ioException = new Alert(Alert.AlertType.ERROR);
            ioException.setTitle("IOException");
            ioException.setContentText("Could not close stream");
            ioException.showAndWait();
        } finally {
            if (dataOutputStream != null){
                try {

                    dataOutputStream.close();

                } catch (IOException e) {
                    Alert ioException = new Alert(Alert.AlertType.ERROR);
                    ioException.setTitle("IOException");
                    ioException.setContentText("Could not close stream");
                    ioException.showAndWait();
                }
            }
        }

    }

    private static Color intToColor(int color) {
        double red = ((color >> 16) & 0x000000FF) / 255.0;
        double green = ((color >> 8) & 0x000000FF) / 255.0;
        double blue = (color & 0x000000FF) / 255.0;
        double alpha = ((color >> 24) & 0x000000FF) / 255.0;
        return new Color(red, green, blue, alpha);
    }

    private static int colorToInt(Color color) {
        int red = ((int) (color.getRed() * 255)) & 0x000000FF;
        int green = ((int) (color.getGreen() * 255)) & 0x000000FF;
        int blue = ((int) (color.getBlue() * 255)) & 0x000000FF;
        int alpha = ((int) (color.getOpacity() * 255)) & 0x000000FF;
        return (alpha << 24) + (red << 16) + (green << 8) + blue;
    }
}
