/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 9 - Final Project
 * Name: Jace Olson
 * Created: 2/8/19
 */

package sample;

import javafx.scene.paint.Color;

@FunctionalInterface
public interface Transformable {
    Color apply(int y, Color c);
}
