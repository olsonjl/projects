/*
 * Course: CS1021 051
 * Winter 2018
 * Lab 9 - Final Project
 * Name: Jace Olson
 * Created: 2/8/19
 */

package sample;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Path;

public class Controller {

    @FXML
    Button showFilter;

    @FXML
    Stage stage;

    @FXML
    ImageView imageView;

    @FXML
    Button openButton;

    @FXML
    Button reloadButton;

    @FXML
    Button saveButton;

    @FXML
    Button grayscaleButton;

    @FXML
    Button negativeButton;

    @FXML
    Button redButton;

    private FileChooser fileChooser = new FileChooser();
    private ImageIO io = new ImageIO();
    private Image origImage;


    private Path imagePath;

    private Transformable negative = (y, c) -> new Color(1 - c.getRed(), 1 - c.getGreen(), 1 - c.getBlue(), 1);
    private Transformable greyScale = (y, c) -> new Color((c.getRed() * 0.2126 + c.getGreen() * 0.7152 + c.getBlue() * 0.0722),
            (c.getRed() * 0.2126 + c.getGreen() * 0.7152 + c.getBlue() * 0.0722), (c.getRed() * 0.2126 + c.getGreen() * 0.7152 + c.getBlue()
            * 0.0722), 1);

    private Transformable red = (y, c) -> new Color(c.getRed(), 0, 0, 1);

    Stage kernelStage;

    public void setKernelStage(Stage stage) {
        this.kernelStage = stage;
    }

    public void load(ActionEvent event) {

        FileChooser file = new FileChooser();
        file.setTitle("Choose an image");
        file.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image files", "*.jpg", "*.gif", "*.png", "*.tiff", "*.msoe", "*bmsoe"));
        imagePath = Path.of(file.showOpenDialog(stage).toURI());
         /*   imagePath = Path.of(fileChooser.showOpenDialog(stage).toURI());
            loadImage = ioImage.read(imagePath);
            imageView.setImage(loadImage);
*/
        String imgFileString = imagePath.toString();
        int periodIndex = imgFileString.indexOf(".");

        String subImageFile = imgFileString.substring(periodIndex);

        if (subImageFile.equals(".gif") || subImageFile.equals(".jpg") || subImageFile.equals(".png") || subImageFile.equals(".tiff") || subImageFile.equals(".msoe") || subImageFile.equals(".bmsoe")) {
            Image image = io.read(imagePath);
            imageView.setImage(image);
            origImage = imageView.getImage();
        } else {
            Alert ioAlert = new Alert(Alert.AlertType.ERROR);
            ioAlert.setTitle("Error");
            ioAlert.setContentText("There was an error");
            ioAlert.setHeaderText("Error");

            ioAlert.showAndWait();
        }

    }

    public void reload(ActionEvent event) {
        imageView.setImage(origImage);
    }

    public void save(ActionEvent event) {
        File file = fileChooser.showSaveDialog(null);
        Path saveImagePath = file.toPath();
        if (saveImagePath != null) {
            ImageIO.write(imageView.getImage(), saveImagePath);
        }

    }

    private static Image transformImage(Image image, Transformable transform) {
        WritableImage outImage = new WritableImage((int) image.getWidth(), (int) image.getHeight());
        PixelReader pixelReader = image.getPixelReader();
        PixelWriter pixelWriter = outImage.getPixelWriter();

        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                Color c = pixelReader.getColor(x, y);
                Color greyImage = transform.apply(y, c);
                pixelWriter.setColor(x, y, greyImage);



              /*  Color pictureColor = pixelReader.getColor(x, y);
                double grayBlue = 0.0722 * pictureColor.getBlue();
                double grayRed = 0.2126 * pictureColor.getRed();
                double grayGreen = 0.7152 * pictureColor.getGreen();
                Color grayImage = Color.color(grayRed + grayGreen + grayBlue, grayRed +
                        grayGreen + grayBlue, grayRed + grayGreen + grayBlue);
                pixelWriter.setColor(x, y, grayImage); */
            }
        }

        return outImage;
    }

    public void grayscale(ActionEvent event) {

        Image image = imageView.getImage();
        Image outImage = transformImage(image, greyScale);

        imageView.setImage(outImage);
    }

    public void negative(ActionEvent event) {
        Image image = imageView.getImage();
        Image outImage = transformImage(image, negative);

        imageView.setImage(outImage);
    }

    public void red(ActionEvent event) {
        Image image = imageView.getImage();
        Image outImage = transformImage(image, red);

        imageView.setImage(outImage);
    }

    public void redGray(ActionEvent event) {
        Image image = imageView.getImage();
        WritableImage outImage = new WritableImage((int) image.getWidth(), (int) image.getHeight());
        PixelReader pixelReader = image.getPixelReader();
        PixelWriter pixelWriter = outImage.getPixelWriter();


        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                if (y % 2 == 0) {
                    Color c = pixelReader.getColor(x, y);
                    Color greyImage = greyScale.apply(y, c);
                    pixelWriter.setColor(x, y, greyImage);


                    /*Color pictureColor = pixelReader.getColor(x, y);
                    double redBlue = 0;
                    double redRed = pictureColor.getRed();
                    double redGreen = 0;
                    Color redImage = Color.color(redRed, redGreen, redBlue);
                    pixelWriter.setColor(x, y, redImage); */

                } else if (!(y % 2 == 0)) {
                    Color c = pixelReader.getColor(x, y);
                    Color redImage = red.apply(y, c);
                    pixelWriter.setColor(x, y, redImage);


                   /* Color pictureColor = pixelReader.getColor(x, y);
                    double grayBlue = 0.0722 * pictureColor.getBlue();
                    double grayRed = 0.2126 * pictureColor.getRed();
                    double grayGreen = 0.7152 * pictureColor.getGreen();
                    Color grayImage = Color.color(grayRed + grayGreen + grayBlue, grayRed +
                            grayGreen + grayBlue, grayRed + grayGreen + grayBlue);
                    pixelWriter.setColor(x, y, grayImage); */
                }
            }
        }
        imageView.setImage(outImage);
    }

    public void showFilter(ActionEvent e) {
        if (showFilter.getText().equals("Show Filter")) {
            kernelStage.show();
            showFilter.setText("Hide Filter");
        } else if (showFilter.getText().equals("Hide Filter")) {

            kernelStage.hide();
            showFilter.setText("Show Filter");
        }

    }

    public void setImage(Image image) {
        this.imageView.setImage(image);
    }

    public Image getImage() {
        return imageView.getImage();
    }




}
