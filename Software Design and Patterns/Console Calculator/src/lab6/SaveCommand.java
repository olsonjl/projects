package lab6;

public class SaveCommand extends CalculatorCommand {
    public SaveCommand(Calculator c) {
        super(c);
    }

    private String memory = "";
    private String accumulator = "";
    public void execute(char c) {
        memory = calculator.getMemory();
        accumulator = calculator.getAccumulator();
        calculator.saveToMemory();
    }

    public void unexecute() {
        calculator.setMemory(memory);
        calculator.setAccumulator(accumulator);
    }
}
