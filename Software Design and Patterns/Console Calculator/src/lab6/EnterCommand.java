package lab6;

public class EnterCommand extends CalculatorCommand {
    public EnterCommand(Calculator c) {
        super(c);
    }

    private String accumulator = "";
    public void execute(char c) {
        accumulator = calculator.getAccumulator();
        calculator.enter();
    }

    public void unexecute() {
        calculator.setAccumulator(accumulator);
    }
}
