package lab6;

public class RecallCommand extends CalculatorCommand {
    public RecallCommand(Calculator c) {
        super(c);
    }

    private String display = "";
    public void execute(char c) {
        display = calculator.getDisplay();
        calculator.recallFromMemory();
    }

    public void unexecute() {
        calculator.setDisplay(display);
    }
}
