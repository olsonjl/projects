package lab6;

public class MinusCommand extends CalculatorCommand {
    public MinusCommand(Calculator c) {
        super(c);
    }

    private String accumulator = "";
    private String display = "";
    public void execute(char c) {
        accumulator = calculator.getAccumulator();
        display = calculator.getDisplay();
        calculator.minus();
    }

    public void unexecute() {
        calculator.setAccumulator(accumulator);
        calculator.setDisplay(display);
    }
}
