package lab6;

public class TimesCommand extends CalculatorCommand {
    public TimesCommand(Calculator c) {
        super(c);
    }

    private String display = "";
    private String accumulator = "";
    public void execute(char c) {
        display = calculator.getDisplay();
        accumulator = calculator.getAccumulator();
        calculator.times();
    }

    public void unexecute() {
        calculator.setAccumulator(accumulator);
        calculator.setDisplay(display);
    }
}
