package lab6;

public class AppendDigitCommand extends CalculatorCommand {
    public AppendDigitCommand(Calculator c) {
        super(c);
    }

    private String display = "";
    public void execute(char c) {
        display = calculator.getDisplay();
        calculator.appendDigit(c);
    }



    public void unexecute() {
        calculator.setDisplay(display);
    }
}
