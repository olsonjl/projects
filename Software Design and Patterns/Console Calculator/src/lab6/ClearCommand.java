package lab6;

public class ClearCommand extends CalculatorCommand {
    public ClearCommand(Calculator c) {
        super(c);
    }

    private String display = "";
    private String accumulator = "";
    private String memory = "";
    public void execute(char c) {
        display = calculator.getDisplay();
        accumulator =calculator.getAccumulator();
        memory = calculator.getMemory();
        calculator.clear();
    }

    public void unexecute() {
        calculator.setDisplay(display);
        calculator.setAccumulator(accumulator);
        calculator.setMemory(memory);
    }

}
