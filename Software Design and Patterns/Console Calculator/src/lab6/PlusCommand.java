package lab6;

public class PlusCommand extends CalculatorCommand {
    public PlusCommand(Calculator c) {
        super(c);
    }

    private String display = "";
    private String accumulator = "";


    public void execute(char c) {
        display = calculator.getDisplay();
        accumulator = calculator.getAccumulator();
        calculator.plus();
    }

    public void unexecute() {
        calculator.setAccumulator(accumulator);
        calculator.setDisplay(display);
    }
}
