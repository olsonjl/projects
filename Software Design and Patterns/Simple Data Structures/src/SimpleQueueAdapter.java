/*
* Jace Olson
* Exercise 1
* 12/6/2019
 */

public class SimpleQueueAdapter implements QueueInterface {
    private SimpleLinkedList<String> items = new SimpleLinkedList<String>();

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public void add(String nextItem) {
        items.addLast(nextItem);
    }

   /* public void dump() {
        System.out.println("Items on queue:");
        for(int i = 0; i < items.size(); ++i)
            System.out.println("  " + items.get(i));
        System.out.println("---");
    }
    */

    public String get() {
        return items.first();
    }
}
