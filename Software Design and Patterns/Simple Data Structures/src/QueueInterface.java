/*
 * Jace Olson
 * Exercise 1
 * 12/6/2019
 */

public interface QueueInterface {
        public boolean isEmpty();
        public void add(String nextItem);

        public String get();

}
