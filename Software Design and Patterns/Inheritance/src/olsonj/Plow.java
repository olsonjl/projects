/*
 * Course: SE2811
 * Winter 2019
 * Lab 1: Inheritance
 * Name: Jace Olson
 * Created: 12/3/2019
 */

package olsonj;

/**
 * requires the dumptruck, pickup, and convertible to implement these methods
 * and have these attributes
 */
public interface Plow {

    boolean plowDown = false;
    double plowWidth = 0;

    public boolean raisePlow();

    public boolean lowerPlow();


}
