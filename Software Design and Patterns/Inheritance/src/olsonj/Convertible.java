/*
 * Course: SE2811
 * Winter 2019
 * Lab 1: Inheritance
 * Name: Jace Olson
 * Created: 12/3/2019
 */

package olsonj;


/**
 * handles the unique convertible methods
 */
public class Convertible extends Car implements Plow {

    private boolean roofDown = false;
    private double plowWidth;
    private boolean plowDown = false;

    /**
     * creates a convertible object
     * @param name of the convertible
     * @param numWheels on the convertible
     * @param weight of the convertible
     * @param plowWidth of the convertible
     */
    public Convertible(String name, int numWheels, double weight, double plowWidth){
        super(name, numWheels, weight);
        this.plowWidth = plowWidth;

    }

    /**
     * lowers the roof
     * @return true if it was lowered, false if was already down
     */
    public boolean lowerRoof(){
        if(!roofDown){
            roofDown = true;
            return true;
        } else {
            return false;
        }

    }

    /**
     * raised the roof
     * @return false if the roof was raised, true if was already up
     */
    public boolean raiseRoof(){
        if(roofDown){
            roofDown = false;
            return false;
        } else {
            return true;
        }
    }

    /**
     * raises te plow
     * @return false if the plow was raised,
     * true if there is no plow or if it was already up
     */
    @Override
    public boolean raisePlow(){
        if (plowWidth == 0){
            System.out.println("no plow ");
            return true;
        }else if(plowDown){
            plowDown = false;
            return false;
        } else {
            return true;
        }
    }

    /**
     * lowers the plow
     * @return true if the plow lowers, false if there is no
     * plow or if it was already lowered
     */
    @Override
    public boolean lowerPlow() {
        if(plowWidth == 0){
            System.out.println("no plow ");
            return false;
        }else if(!plowDown){
            plowDown = true;
            return true;
        } else {
            return false;
        }
    }
}

