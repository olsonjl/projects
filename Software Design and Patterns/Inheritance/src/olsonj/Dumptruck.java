/*
 * Course: SE2811
 * Winter 2019
 * Lab 1: Inheritance
 * Name: Jace Olson
 * Created: 12/3/2019
 */

package olsonj;

/**
 * creates a dumptruck vehicle and handles all unique features
 */
public class Dumptruck extends Vehicle implements Plow {

    private boolean plowDown = false;
    private double plowWidth;
    private double loadCapacity;
    private boolean bucketUp;

    /**
     * creates a dumptruck
     * @param name of the dumptruck
     * @param numWheels it has
     * @param weight of the dumptruck
     * @param plowWidth of the dumptruck
     * @param loadCapacity of the dumptruck
     */
    public Dumptruck(String name, int numWheels, double weight, double plowWidth, double loadCapacity){
        super(name, numWheels, weight);
        this.plowWidth = plowWidth;
        this.loadCapacity = loadCapacity;
    }

    /**
     * lowers the load
     */
    public void lowerLoad(){
        if(bucketUp){
            bucketUp = false;
        }
    }

    /**
     * raises the load
     */
    public void raiseLoad(){
        if(!bucketUp){
           bucketUp = true;
        }
    }

    /**
     * raises the plow
     * @return false if the plow has been raised, true if it was already up
     */
    @Override
    public boolean raisePlow(){
        if(plowDown){
            plowDown = false;
            return false;
        } else {
            return true;
        }

    }

    /**
     * lowers the plow
     * @return true if it was lowered, false if it was already down
     */
    @Override
    public boolean lowerPlow() {
        if(!plowDown){
            plowDown = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * gets the boolean if the bucket is up
     * @return true if it's up
     */
    public boolean getBucketUp(){
        return bucketUp;
    }


    /*
    public boolean raiseRoof(){
        if(roofDown){
            roofDown = false;
            return false;
        } else {
            return true;
        }
    }
    */
    /*
    public boolean lowerRoof(){
        if(!roofDown){
            roofDown = true;
            return true;
        } else {
            return false;
        }

    }
    */
}
