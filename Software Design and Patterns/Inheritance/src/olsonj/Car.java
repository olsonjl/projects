/*
 * Course: SE2811
 * Winter 2019
 * Lab 1: Inheritance
 * Name: Jace Olson
 * Created: 12/3/2019
 */

package olsonj;

/**
 * handles the unique car methods (none)
 */
public class Car extends Vehicle {
    private int numWheels;
    private double weight;

    private double speed = 0;
    private double accel = 0;

    private String name;

    /**
     * creates a car vehicle object
     * @param name of the care
     * @param numWheels the car has
     * @param weight of the car
     */
    public Car(String name, int numWheels, double weight) {
        super(name, numWheels, weight);
    }

 /*
    @Override
    public boolean lowerPlow() {
        if(!plowDown){
            plowDown = true;
            return true;
        } else {
            return false;
        }
    }
    */
/*
    @Override
    public boolean raisePlow(){
        if(plowDown){
            plowDown = false;
            return false;
        } else {
            return true;
        }
    }
    */

 /*
    public boolean raiseRoof(){
        if(roofDown){
            roofDown = false;
            return false;
        } else {
            return true;
        }
    }
    */

 /*
    public boolean lowerRoof(){
        if(!roofDown){
            roofDown = true;
            return true;
        } else {
            return false;
        }

    }
    */
}

