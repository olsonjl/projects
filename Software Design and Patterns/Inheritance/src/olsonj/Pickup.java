/*
* Course: SE2811
* Winter 2019
* Lab 1: Inheritance
* Name: Jace Olson
* Created: 12/3/2019
 */

package olsonj;

/**
 * handles the pickup vehicle
 */
public class Pickup extends Vehicle implements Plow{


    private boolean plowDown;
    private double loadCapacity;
    private double plowWidth;

    /**
     * creates a pickup vehicle object
     * @param name of the truck
     * @param numWheels on the truck
     * @param weight of the truck
     * @param loadCapacity of the truck
     * @param plowWidth of the truck
     */
    public Pickup(String name, int numWheels, double weight, double loadCapacity, double plowWidth){
        super(name, numWheels, weight);
        this.loadCapacity = loadCapacity;
        this.plowWidth = plowWidth;
    }

    /**
     * raises the plow
     * @return false if the plow was raised
     */
    @Override
    public boolean raisePlow(){
        if(plowDown){
            plowDown = false;
            return false;
        } else {
            return true;
        }
    }

    /**
     * lowers the plow
     * @return boolean for the plow
     */
    @Override
    public boolean lowerPlow() {
        if(!plowDown){
            plowDown = true;
            return true;
        } else {
            return false;
        }
    }

    /**
     * gets load capacity
     * @return the load capacity
     */
    public double getLoadCapacity(){
        return loadCapacity;
    }

    /**
     * gets the plow width
     * @return the plow width
     */
    public double getPlowWidth(){
        return plowWidth;
    }

     /*
    public boolean raiseRoof(){
        if(roofDown){
            roofDown = false;
            return false;
        } else {
            return true;
        }
    }
    */
     /*
    public boolean lowerRoof(){
        if(!roofDown){
            roofDown = true;
            return true;
        } else {
            return false;
        }

    }
    */
}
