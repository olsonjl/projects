/*
 * Course: SE2811
 * Winter 2019
 * Lab 1: Inheritance
 * Name: Jace Olson
 * Created: 12/3/2019
 */

package olsonj;

/**
 * handles the generic car methods
 */
public abstract class Vehicle {

    private int numWheels;
    private double weight;
    private String name;

    private double speed = 0;
    private double accel = 0;
    private boolean startFlag = false;

    /**
     * creates a vehicle object
     * @param name of the vehicle
     * @param numWheels on the vehicle
     * @param weight of the vehicle
     */
    public Vehicle(String name, int numWheels, double weight){
        this.name = name;
        this.numWheels = numWheels;
        this.weight = weight;

    }

    /**
     * goes backward
     * @param speed in which to go backward
     * @param accel in the backward direction
     */
    public void goBackward(double speed, double accel) {
        this.speed = speed;
        this.accel = accel;
    }

    /**
     * goes forward
     * @param speed in which to fo forward
     * @param accel in the forward direction
     */
    public void goForward(double speed, double accel) {
        this.speed = speed;
        this.accel = accel;
    }

    /**
     * stops the vehicle
     */
    public void stop() {
        speed = 0;
        accel = 0;
    }

    /**
     * turns the vehicle off
     * @return
     */
    public boolean turnOff(){
        if(startFlag){
            startFlag = false;
        }
        return startFlag;
    }

    /**
     * starts the vehicle
     * @return true if the vehicle has been started, false if it was already on
     */
    public boolean start() {
        if(!startFlag){
            startFlag = true;
        }

        return startFlag;
    }

    /**
     * gets the accleration
     * @return the accel
     */
    public double getAccel() {
        return accel;
    }

    /**
     * get the speed
     * @return the speed
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * gets the weight of the car
     * @return the weight
     */
    public double getWeight() {
        return weight;
    }

    /**
     * gets the number of wheels on the vehicle
     * @return the number of wheels
     */
    public int getNumWheels() {
        return numWheels;
    }

    /**
     * gets the name of the vehicle
     * @return String of the name
     */
    public String getName() {
        return name;
    }



}
