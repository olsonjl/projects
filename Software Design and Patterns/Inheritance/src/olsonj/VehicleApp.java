/*
 * Course: SE2811
 * Winter 2019
 * Lab 1: Inheritance
 * Name: Jace Olson
 * Created: 12/3/2019
 */

package olsonj;

/**
 * tests each vehicle type
 */
public class VehicleApp {
    public static void main(String[] args) {
        double plowWidth = 2;
        double truckCapacity = 600;
        double dumpCapacity = 3000;
        double carWeight = 1500;
        double truckWeight = 2500;
        double dumpTruckWeight = 5000;
        double convertibleWeight = 1250;
        int dumpNumWheel = 6;


        Car car = new Car("car", 4, carWeight);
        System.out.println("Created a car.");
        testVehicle(car);
        System.out.println("-------------------------------------------------------");
        car.raiseRoof();
        car.lowerRoof();
        car.lowerPlow();
        car.raisePlow();

        Pickup pickup = new Pickup("pickup", 4, truckWeight, truckCapacity, plowWidth);
        System.out.println("Created a pickup.");
        testVehicle(pickup);
        System.out.println("Load Capacity: " + truckCapacity);
        System.out.println("-------------");
        System.out.println("Lowering the plow if true: " + pickup.lowerPlow());
        System.out.println("Raising the plow if false: " + pickup.raisePlow());
        System.out.println("--------------------------------------------------------");
        System.out.println("Cannot raise or lower roof since it's not a convertible.");
        pickup.raiseRoof();
        pickup.lowerRoof();

        Dumptruck dumpTruck = new Dumptruck("dump", dumpNumWheel, dumpTruckWeight, plowWidth, dumpCapacity);
        System.out.println("Created a dumptruck.");
        testVehicle(dumpTruck);
        System.out.println("Load Capacity: " + dumpCapacity);
        System.out.println("-------------------");
        System.out.println("Lowering the plow if true: " + dumpTruck.lowerPlow());
        System.out.println("Raising the plow if false: " + dumpTruck.raisePlow());
        System.out.println("-------------------");
        dumpTruck.lowerLoad();
        System.out.println("Lowering the load if false: " + dumpTruck.getBucketUp());
        dumpTruck.raiseLoad();
        System.out.println("Raising the load if true: " + dumpTruck.getBucketUp());
        System.out.println("----------------------------------------------------------");
        dumpTruck.raiseRoof();
        dumpTruck.lowerRoof();

        Convertible convertible = new Convertible("conv", 4, convertibleWeight, plowWidth);
        System.out.println("Created a convertible.");
        testVehicle(convertible);
        System.out.println("-------------");
        System.out.println("Lowering the plow if true: " + convertible.lowerPlow());
        System.out.println("Raising the plow if false: " + convertible.raisePlow());
        System.out.println("--------------------------------------------------------");
        System.out.println("Testing if plowWidth = 0 (no plow)");
        Convertible c = new Convertible("convNoPlow", 4, convertibleWeight, 0);
        System.out.println("Lowering the plow if true: " + c.lowerPlow());
        System.out.println("Raising the plow if false: " + c.raisePlow());
        System.out.println("----------------------------------------");
        System.out.println("Lowering roof if true: " + convertible.lowerRoof());
        System.out.println("Raising roof if false: " + convertible.raiseRoof());
        
    }

    /**
     * will test any kind of vehicle
     *
     * @param vehicle of any kind
     */
    public static void testVehicle(Vehicle vehicle) {
        System.out.println("Name: " + vehicle.getName());
        System.out.println("Number of Wheels: " + vehicle.getNumWheels());
        System.out.println("Weight: " + vehicle.getWeight());
        System.out.println("Starting the vehicle if true: " + vehicle.start());
        vehicle.goForward(2, 2);
        System.out.println(vehicle.getName() + " is going " + vehicle.getSpeed() + " m/s and acclerating at " +
                vehicle.getAccel() + " m/s^2 forward");
        vehicle.goBackward(1, 1);
        System.out.println(vehicle.getName() + " is going " + vehicle.getSpeed() + " m/s and acclerating at " +
                vehicle.getAccel() + " m/s^2 backward");
        vehicle.stop();
        System.out.println(vehicle.getName() + " is now stopping if speed and accel = 0:  Speed: "
                + vehicle.getSpeed() + " Accel: " + vehicle.getAccel());
        vehicle.turnOff();
        System.out.println(vehicle.getName() + " is now turning off if false: " + vehicle.turnOff());

    }


}
