/*
 * Course:     SE 2811
 * Term:       Winter 2019-20
 * Assignment: Lab 4: Decorators
 * Author: Dr. Yoder and _______
 * Date:
 */
package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The controller for the main window.
 *
 * Also manages the networks.
 */
public class SimpleCanvasController {
    @FXML
    private ToggleGroup network;
    @FXML
    private Canvas canvas;

    @FXML
    private ToggleButton inceptionLike;

    @FXML
    private ToggleButton alexLike;



    private Map<String, NetworkInt> networks = new HashMap<String, NetworkInt>();



    @FXML
    private void showNetwork(ActionEvent actionEvent) {
        ToggleButton source = (ToggleButton)actionEvent.getSource();
        String id = source.getId();
        System.out.println("id = " + id);

        // Clear Canvas: https://stackoverflow.com/q/27203671/1048186
        GraphicsContext context = canvas.getGraphicsContext2D();
        System.out.println("canvas.getWidth() = " + canvas.getWidth());
        System.out.println("canvas.getHeight() = " + canvas.getHeight());
        context.setLineWidth(3);
        context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        if(!networks.containsKey(id)) {
            System.out.println("Warning: Unknown network id:"+id);
        } else if(id.equals("inceptionLike")) {
            System.out.println("DEBUG: 1 Drawing network: "+id);
            NetworkInt network = networks.get(id);
            network.draw(canvas, 50);
            network = new DecoratedNetwork(network, "inceptionLike");
            network.draw(canvas, 50);

        } else if(id.equals("alexLike")){
            System.out.println("DEBUG: Drawing network: "+id);

            NetworkInt network = networks.get(id);
            network.draw(canvas, 50);
            network = new DecoratedNetwork(network, "alexLike");
            network.draw(canvas, 175);

        } else if(id.equals("customLike")){
            System.out.println("DEBUG: Drawing network: " + id);
            NetworkInt network = networks.get(id);
            network.draw(canvas, 50);
            network = new DecoratedNetwork(network, "customLike");
            network.draw(canvas, 100);
        }
    }

    @FXML
    private void initialize() {
        networks.put("alexLike",createAlexNet());
        networks.put("inceptionLike",createInception());
        networks.put("customLike", createCustom());
    }

    /**
     * As client code, use the decorator classes to construct the inception-like network,
     * as described in the lab.
     * @return network The network created.
     */
    private NetworkInt createInception() {
        NetworkInt incep = new Inception();
        return incep;

    }

    /**
     * As client code, use the decorator classes to construct the AlexNet-like network,
     * as described in the lab.
     * @return network The network created.
     */
    private NetworkInt createAlexNet() {
        NetworkInt alexNet = new AlexNet();
        return alexNet;
    }

    private NetworkInt createCustom(){
        NetworkInt customNet = new CustomNet();
        return customNet;
    }





}
