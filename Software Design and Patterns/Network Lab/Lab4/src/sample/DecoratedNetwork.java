package sample;

import javafx.scene.canvas.Canvas;

public class DecoratedNetwork implements NetworkInt{
    private static final int RADIUS = 30;
    protected NetworkInt networkInt;
    private String id = "";
    public DecoratedNetwork(NetworkInt networkInt, String id){
        this.networkInt = networkInt;
        this.id = id;
    }
    public void draw(Canvas canvas, int x){
        if(id.equals("inceptionLike")){
            drawNode(canvas, x + 600, 200);
        } else if(id.equals("alexLike")){
            drawNode(canvas, x + 600, 275);
        } else if(id.equals("customLike")){
            drawNode(canvas, x + 650, 300);
        }

    }

    public void drawNode(Canvas canvas, int x, int y) {
        canvas.getGraphicsContext2D().strokeOval(x - RADIUS, y - RADIUS, 2 * RADIUS, 2 * RADIUS);
    }



}
