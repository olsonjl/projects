package sample;

import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;


public class AlexNet implements NetworkInt {
    private int layerNum = 1;
    private static final int RADIUS = 30;
    public void draw(Canvas canvas, int x){
        drawEdge(canvas, x, 50, x + 150, 50);
        drawEdge(canvas, x, 200, x + 150, 200);
        drawEdge(canvas, x, 350, x + 150, 350);
        drawEdge(canvas, x, 500, x + 150, 500);

        drawEdge(canvas, x + 150, 50, x + 300, 50);
        drawEdge(canvas, x + 150, 200, x + 300, 200);
        drawEdge(canvas, x + 150, 350, x + 300, 350);
        drawEdge(canvas, x + 150, 500, x + 300, 500);

        drawEdge(canvas, x + 300, 50, x + 450, 50);
        drawEdge(canvas, x + 300, 50, x + 450, 200);
        drawEdge(canvas, x + 300, 50, x + 450, 350);
        drawEdge(canvas, x + 300, 50, x + 450, 500);

        drawEdge(canvas, x + 300, 200, x + 450, 50);
        drawEdge(canvas, x + 300, 200, x + 450, 200);
        drawEdge(canvas, x + 300, 200, x + 450, 350);
        drawEdge(canvas, x + 300, 200, x + 450, 500);

        drawEdge(canvas, x + 300, 350, x + 450, 50);
        drawEdge(canvas, x + 300, 350, x + 450, 200);
        drawEdge(canvas, x + 300, 350, x + 450, 350);
        drawEdge(canvas, x + 300, 350, x + 450, 500);

        drawEdge(canvas, x + 300, 500, x + 450, 50);
        drawEdge(canvas, x + 300, 500, x + 450, 200);
        drawEdge(canvas, x + 300, 500, x + 450, 350);
        drawEdge(canvas, x + 300, 500, x + 450, 500);

        drawEdge(canvas, x + 450, 50, x + 600, 125);
        drawEdge(canvas, x + 450, 50, x + 600, 275);
        drawEdge(canvas, x + 450, 50, x + 600, 425);

        drawEdge(canvas, x + 450, 200, x + 600, 125);
        drawEdge(canvas, x + 450, 200, x + 600, 275);
        drawEdge(canvas, x + 450, 200, x + 600, 425);

        drawEdge(canvas, x + 450, 350, x + 600, 125);
        drawEdge(canvas, x + 450, 350, x + 600, 275);
        drawEdge(canvas, x + 450, 350, x + 600, 425);

        drawEdge(canvas, x + 450, 500, x + 600, 125);
        drawEdge(canvas, x + 450, 500, x + 600, 275);
        drawEdge(canvas, x + 450, 500, x + 600, 425);

        drawEdge(canvas, x + 600, 125, 775, 275);
        drawEdge(canvas, x + 600, 275, 775, 275);
        drawEdge(canvas, x + 600, 425, 775, 275);

        drawIdentityLayerAlex(canvas, x);
        drawConvLayerAlex(canvas, x);
        drawConvLayerAlex(canvas, x);
        drawConvLayerAlex(canvas, x);
        drawThreeNode(canvas, x);
        layerNum = 1;
    }

    public void drawEdge(Canvas canvas, int x1, int y1, int x2, int y2) {
        GraphicsContext context = canvas.getGraphicsContext2D();
        Point2D p1 = new Point2D(x1, y1);
        Point2D p2 = new Point2D(x2, y2);
        Point2D direction = p2.subtract(p1).normalize();
        Point2D radius = direction.multiply(RADIUS);
        Point2D start = p1.add(radius);
        Point2D end = p2.subtract(radius);
        context.strokeLine(start.getX(), start.getY(), end.getX(), end.getY());
        drawArrow(canvas, x1, y1, x2, y2);

    }

    public void drawConvLayerAlex(Canvas canvas, int x){
        drawNode(canvas, x + (150 * layerNum), 50);
        drawNode(canvas, x + (150 * layerNum), 200);
        drawNode(canvas, x + (150 * layerNum), 350);
        drawNode(canvas, x + (150 * layerNum), 500);
        layerNum +=1;
    }

    public void drawNode(Canvas canvas, int x, int y) {
        canvas.getGraphicsContext2D().strokeOval(x - RADIUS, y - RADIUS, 2 * RADIUS, 2 * RADIUS);
    }

    public void drawIdentityLayerAlex(Canvas canvas, int x) {
        drawNode(canvas, x, 50);
        drawNode(canvas, x, 200);
        drawNode(canvas, x, 350);
        drawNode(canvas, x, 500);
    }

    public void drawThreeNode(Canvas canvas, int x){
        drawNode(canvas, x + (150 * layerNum), 125);
        drawNode(canvas, x + (150 * layerNum), 275);
        drawNode(canvas, x + (150 * layerNum), 425);
    }

    public void drawArrow(Canvas canvas, int x1, int y1, int x2, int y2) {
        GraphicsContext gc = canvas.getGraphicsContext2D();

        double angle = Math.atan2((y2 - y1), (x2 - x1)) - Math.PI / 2.0;
        double sin = Math.sin(angle);
        double cos = Math.cos(angle);
        double arrowY;
        double arrowX;

        if(y1>y2){
            arrowY= -1*Math.abs(30*Math.cos(angle));
            arrowX = Math.abs(30*Math.sin(angle));

        }else{
            arrowY= Math.abs(30*Math.cos(angle));
            arrowX = Math.abs(30*Math.sin(angle));
        }

        double newx1 = (- 1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * 15 + x2;
        double newy1 = (- 1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * 15 + y2;
        double newx2 = (1.0 / 2.0 * cos + Math.sqrt(3) / 2 * sin) * 15 + x2;
        double newy2 = (1.0 / 2.0 * sin - Math.sqrt(3) / 2 * cos) * 15 + y2;

        gc.strokeLine(x2-arrowX,y2-arrowY,newx1-arrowX,newy1-arrowY);
        gc.strokeLine(x2-arrowX,y2-arrowY,newx2-arrowX,newy2-arrowY);

    }

}
