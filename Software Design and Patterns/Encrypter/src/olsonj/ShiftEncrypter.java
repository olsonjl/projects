/*
 * Jace Olson
 * 1/7/2020
 * SE 2811
 * Strategy-based Encryption
 */

package olsonj;

/**
 * handles the shifting encryption methods
 */
public class ShiftEncrypter implements Encrypter {
    private int amount;

    /**
     * creates an instance of a shiftEncrypter
     * @param amount of numbers to shift
     */
    public ShiftEncrypter(int amount){
        this.amount = amount;
    }

    /**
     * encrypts the message by shifting them a certain amount
     * @param byteArray to encrypt
     * @return a new shifted array
     */
    public byte[] encrypt(byte[] byteArray) {
        byte[] shiftArray = new byte[byteArray.length];

        for(int i = 0; i < byteArray.length; i++){

            if(byteArray[i] + amount < 256){
                int shift = byteArray[i];
                shift = shift + amount;
                shiftArray[i] = (byte) shift;
             //   shiftArray[i] = Byte.parseByte(byteArray[i] + amount + "");
            } else {
               /* if(byteArray[i] + 1  < 256){
                    byteArray[i] = Byte.parseByte(byteArray[i] + 1 + "");
                    amount--;
                } else if(byteArray[i] + 1 > 256){
                    byteArray[i] = 0;
                    amount--;
                }
                shiftArray[i] = byteArray[i];
                */
                int ascii = byteArray[i];
                while(ascii + 1 <= 256){
                    byteArray[i] = Byte.parseByte(byteArray[i] + 1 + "");
                    amount--;
                }
                if(amount != 0){
                    byteArray[i] = 0;
                    amount--;
                }
                for(int j = 0; j < amount; j ++){
                    byteArray[i] = Byte.parseByte(byteArray[i] + 1 + "");

                }
                shiftArray[i] = byteArray[i];
            }
        }
        return shiftArray;
    }

    /**
     * decrypts the message through shifting
     * @param byteArray to decrypt
     * @return the new array
     */
    public byte[] decrypt(byte[] byteArray){
        byte[] shiftArray = new byte[byteArray.length];

        for(int i = 0; i < byteArray.length; i++){
            if(byteArray[i] - amount > 0){
                shiftArray[i] = Byte.parseByte(byteArray[i] - amount + "");
            } else {
                while(byteArray[i] - 1 >= 0){
                    byteArray[i] = Byte.parseByte(byteArray[i] - 1 + "");
                    amount--;
                }
                if(amount != 0){
                    byteArray[i] = Byte.parseByte(256 + "");
                    amount--;
                }
                for(int j = 0; j < amount; j++){
                    byteArray[i] = Byte.parseByte(byteArray[i] - 1 + "");
                }
                shiftArray[i] = byteArray[i];
            }
        }
        return shiftArray;
    }

    /**
     * gets the type of "cryption"
     * @return a string of the type
     */
    public String getType(){
        return "shift";
    }

}
