/*
 * Jace Olson
 * 1/7/2020
 * SE 2811
 * Strategy-based Encryption
 */

package olsonj;

/**
 * handles the crypstick methods
 */
public class CrypStick {

    private Media media;
    private String encryptType;
    private String message;
    private Encrypter encrypter;

    /**
     * creates a CrypStick object
     * @param m media
     */
    public CrypStick(Media m){
        media = m;
    }

    /**
     * gets the media
     * @return the media
     */
    protected Media getMedia(){
        return media;
    }

    /**
     * gets the message
     * @return returns the message
     */
    public String getMessage(){
        return message;
    }

    /**
     * sets the encryption type
     * @param e encrypter to use
     */
    public void setEncryption(Encrypter e){
        encrypter = e;
        this.encryptType = e.getType();
    }

    /**
     * sets the message
     * @param s of the message
     */
    public void setMessage(String s){
        message = s;
    }

    /**
     * sets the media
     * @param media to use
     */
    public void setMedia(Media media){
        this.media = media;
    }

    /**
     * gets the encrypter
     * @return the encrypter
     */
    public Encrypter getEncrypter(){
        return encrypter;
    }

    /**
     * get the encryption type
     * @return a string of the type
     */
    public String getEncryptType(){
        return encryptType;
    }
}
