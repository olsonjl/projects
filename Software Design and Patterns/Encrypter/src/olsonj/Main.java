/*
* Jace Olson
* 1/7/2020
* SE 2811
* Strategy-based Encryption
 */
package olsonj;

import java.util.Scanner;
import java.nio.charset.StandardCharsets;

/**
 * This main class runs the program
 * Note: Brendan helped guide me through connecting everything
 * because I was very confused as to how the CrypStick was used.
 * Only was helped through the main class.
 */
public class Main {

    private static Scanner in = new Scanner(System.in);
    private static CrypStick cs = new CrypStick(new Media(new byte[0]));

    /**
     * run the program
     * @param args for the program
     */
    public static void main(String[] args) {
        String type = getType();
        getMethod();
        getMessage();
        String message = cs.getMessage();
        String method = cs.getEncryptType();
        if(method.equals("rev")){
            cs.setEncryption(new ReverseEncrypter(message.getBytes()));
        }
        if (type.equals("e")){
            encrypt(message);
        } else if (type.equals("d")){
            decrypt(message);
        }
    }

    /**
     * gets whether to encrypt or decrypt the message
     * @return the cryption type
     */
    public static String getType(){
        System.out.print("E)ncrypt or D)ecrypt: ");
        String cryption = in.next().toLowerCase();
        return cryption;
    }

    /**
     * gets the method of encrypting or decrypting the message
     */
    public static void getMethod(){
        System.out.print("Method (rev, shift, xor): ");
        String method = in.next().toLowerCase();
        Encrypter e = null;

        if (method.equals("rev")){
            e = new ReverseEncrypter(new byte[0]);
        } else if(method.equals("shift")){
            System.out.print("Shift amount: ");
            e = new ShiftEncrypter(in.nextInt());
        } else {
            System.out.print("Key: ");
            e = new XOREncrypter(in.next().toLowerCase().getBytes());
        }

        cs.setEncryption(e);
    }

    /**
     * gets the message to encrypt or decrypt
     */
    public static void getMessage(){
        System.out.println("Message: ");
        String line = "";
        String message = line;
        while(in.hasNext()){
            line = in.nextLine();
            if(!line.isEmpty() && in.hasNextLine()){
                message += line + "\n";
            } else if(!line.isEmpty()){
                message += line;
            }
        }
        cs.setMessage(message);
    }

    /**
     * encrypts the message with the right method
     * @param message to encrypt
     */
    public static void encrypt(String message){
        message = message + "\n";
        cs.setMedia(new Media(message.getBytes(StandardCharsets.UTF_8)));
        cs.setMedia(new Media(cs.getEncrypter().encrypt(cs.getMedia().get())));
        System.out.println(cs.getMedia().toString().trim());
    }

    /**
     * decrypts the message with the right method
     * @param message to decrypt
     */
    public static void decrypt(String message){
        cs.getMedia().set(message);
        cs.getMedia().set(cs.getEncrypter().decrypt(cs.getMedia().get()));
        String result = "";
        for (int i = 0; i < cs.getMedia().get().length; i++){
            result = new String(cs.getMedia().get(), StandardCharsets.UTF_8);
        }
        System.out.println(result);
    }


}


