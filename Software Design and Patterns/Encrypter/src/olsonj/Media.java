/*
 * Jace Olson
 * 1/7/2020
 * SE 2811
 * Strategy-based Encryption
 */

package olsonj;

/**
 * Handles the media
 */
public class Media {
    private byte[] bytes;

    /**
     * creates a media object
     * @param b array
     */
    public Media(byte[] b){
        this.bytes = b;
    }

    /**
     * gets the byte array
     * @return byte array
     */
    public byte[] get(){
        return bytes;
    }

    /**
     * sets the byte array
     * @param b to be set
     */
    public void set(byte[] b){
        bytes = b;
    }

    /**
     * sets the string
     * @param s to be set
     */
    public void set(String s){
        s = s.replaceAll("\\s+", "");
        s = s + "\n";
        byte[] temp = new byte[s.length()/2];
        this.bytes = new byte[temp.length];
        for(int i = 0; i < temp.length; i++){
            int index = i * 2;
            int j = Integer.parseInt(s.substring(index, index + 2), 16);
            bytes[i] = (byte) j;
        }
    }

    /**
     * makes the array into a string
     * @return a string
     */
    @Override
    public String toString(){
        String message = "";
        for(byte b :bytes){
            message += (String.format("%02X", b)+ " ").toLowerCase();
        }
        return message;
    }
}
