/*
 * Jace Olson
 * 1/7/2020
 * SE 2811
 * Strategy-based Encryption
 */

package olsonj;

/**
 * Encrypter interface
 */
public interface Encrypter {
    /**
     * decrypts the array
     * @param bytes array to decrypt
     * @return a new array
     */
    public byte[] decrypt(byte[] bytes);

    /**
     * encrypts the array
     * @param bytes array to encrypt
     * @return a new array
     */
    public byte[] encrypt(byte[] bytes);

    /**
     * gets the type
     * @return String of the type
     */
    public String getType();

}
