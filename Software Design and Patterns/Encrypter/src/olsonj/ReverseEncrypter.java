/*
 * Jace Olson
 * 1/7/2020
 * SE 2811
 * Strategy-based Encryption
 */

package olsonj;

/**
 * handles the reverse encryption methods
 */
public class ReverseEncrypter implements Encrypter{
    private byte[] message;

    /**
     * creatse a reverseEncrypter object
     * @param message to encrypt or decrypt
     */
    public ReverseEncrypter(byte[] message){
        this.message = message;
    }

    /**
     * encrypts the array by reversing the order
     * @param byteArray to be reversed
     * @return the new array after encryption
     */
    public byte[] encrypt(byte[] byteArray) {
        byte[] reverseArray = new byte[byteArray.length];
        for (int i = 0; i < byteArray.length; i++) {
            reverseArray[(reverseArray.length - 1) - i] = byteArray[i];
        }
        return reverseArray;
    }
    /**
     * decrypts the array by reversing the order
     * @param byteArray to be reversed
     * @return the new array after decryption
     */
    public byte[] decrypt(byte[] byteArray) {
        byte[] decryptArray = new byte[byteArray.length];
        for(int i = 0; i < byteArray.length; i++){
            decryptArray[(byteArray.length - 1) - i] = byteArray[i];
        }
        return decryptArray;
    }

    /**
     * gets the type of "cryption"
     * @return a string of the type
     */
    public String getType(){
        return "rev";
    }

}
