/*
 * Jace Olson
 * 1/7/2020
 * SE 2811
 * Strategy-based Encryption
 */

package olsonj;

/**
 * Handles the XOR encryption
 */
public class XOREncrypter implements Encrypter {
    private byte[] key;

    /**
     * creates a XOREncrypter object
     * @param key to use to encrypt
     */
    public XOREncrypter(byte[] key){
        this.key = key;
    }

    /**
     * encrypts the array with xor
     * @param byteArray to encrypt
     * @return new array after encryption
     */
    public byte[] encrypt(byte[] byteArray){

        int count = 0;
        byte[] newKey = new byte[key.length];
        while(newKey.length < byteArray.length){
            newKey = new byte[newKey.length * 2];
            for (int j = 0; j < newKey.length; j++){
                if(count > key.length - 1){
                    count = 0;
                }
                newKey[j] = key[count];
                count++;
            }
        }
        byte[] xorArray = new byte[byteArray.length];
        for(int i = 0; i < byteArray.length; i++){

            int value = byteArray[i]^newKey[i];
            xorArray[i] = (byte) value;
        }
        return xorArray;
    }

    /**
     * decrypts the array
     * @param byteArray to decrypt
     * @return a new array after decryption
     */
    public byte[] decrypt(byte[] byteArray){
        int count = 0;
        byte[] newKey = new byte[key.length];
        while(newKey.length < byteArray.length){
            newKey = new byte[newKey.length * 2];
            for (int j = 0; j < newKey.length; j++){
                if(count > key.length - 1){
                    count = 0;
                }
                newKey[j] = key[count];
                count++;
            }
        }
        byte[] xorArray = new byte[byteArray.length];
        for(int i = 0; i < byteArray.length; i++){

            int value = byteArray[i]^newKey[i];
            xorArray[i] = (byte) value;
        }
        return xorArray;
    }

    /**
     * gets the type of encryption
     * @return a string of the type
     */
    public String getType(){
        return "xor";
    }
}
