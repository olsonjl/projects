"""
- CS2911 - 011
- Fall 2018
- Lab 5
- Names:
  - Jeromy Schultz
  - Jace Olson

A simple HTTP server
"""

import socket
import re
import threading
import os
import mimetypes
import datetime


def main():
    """ Start the server """
    http_server_setup(8080)


def http_server_setup(port):
    """
    Start the HTTP server
    - Open the listening socket
    - Accept connections and spawn processes to handle requests

    :param port: listening port number
    """

    num_connections = 1
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_address = ('', port)
    server_socket.bind(listen_address)
    server_socket.listen(num_connections)
    try:
        while True:
            request_socket, request_address = server_socket.accept()
            print('connection from {0} {1}'.format(request_address[0], request_address[1]))
            # Create a new thread, and set up the handle_request method and its argument (in a tuple)
            request_handler = threading.Thread(target=handle_request, args=(request_socket,))
            # Start the request handler thread.
            request_handler.start()
            # Just for information, display the running threads (including this main one)
            # print('threads: ', threading.enumerate())

    # Set up so a Ctrl-C should terminate the server; this may have some problems on Windows
    except KeyboardInterrupt:
        print("HTTP server exiting . . .")
        print('threads: ', threading.enumerate())
        server_socket.close()


def handle_request(data_socket):
    """
    Handle a single HTTP request, running on a newly started thread.

    Closes request socket after sending response.

    Should include a response header indicating NO persistent connection

    :param data_socket:
    :param data_socket: socket representing TCP connection from the HTTP client_socket
    :return: None
    """

    make_http_response(data_socket)


def make_http_response(data_socket):
    """
   Initializes the creation of the HTTP response
    :param data_socket: Socket to send data to
    :return: None
    """

    resource = parse_header(data_socket)
    path = get_path(resource)
    content_length = get_file_size(path)
    send_response(data_socket, content_length, path)


def parse_header(data_socket):
    """
     - Parses through the header of the http response message line by line
     - Checks each line for the status code and the content length if it exists

     :param data_socket: data socket to fetch data from
     :author Jace Olson
     """

    b = b''
    request_dictionary = {}
    run = True
    while run:
        byte_line = next_line(data_socket)
        if b'GET' in byte_line:
            resource = get_resource(byte_line)
        add_to_dictionary(request_dictionary, byte_line)
        b += byte_line
        if byte_line == b'\r\n':
            run = False
    print(request_dictionary)
    return resource


def get_resource(byte_line):
    """
    Gets the resource from the HTML GET request
    :param byte_line: Line that contains the GET request
    :return: The resource as a string
    """

    t_index = byte_line.index(b'T')
    h_index = byte_line.index(b'H')
    resource = byte_line[t_index + 2: h_index - 1].decode()
    if resource == '/':
        resource = '/index.html'
    return resource


def add_to_dictionary(dictionary, byte_line):
    """
    Parses a header in the get request and adds it to the dictionary accordingly
    :param dictionary: Dictionary to add to
    :param byte_line: Header line of GET request
    :return: None
    """
    byte_string = byte_line.decode()
    if ":" in byte_string:
        key_pair = byte_string.split(":", 1)
        dictionary[key_pair[0]] = key_pair[1]
    elif "GET" in byte_string:
        key_pair = byte_string.split("/", 1)
        dictionary[key_pair[0]] = key_pair[1]


def get_path(resource):
    """
    Returns the path of the resource

    :param resource:
    :return: The string path of the resource found in the html get request
    """
    return '.' + resource


def send_response(data_socket, content_length, path):
    """
    Sends the HTTP response to the client
    :param data_socket: Socket to send data to
    :param content_length: Length of the payload data in the requested file
    :param path: Path of the requested file
    :return: None
    """

    header = response_header(path)
    data_socket.sendall(header + b'\r\n')
    payload = read_payload(path, content_length)
    data_socket.sendall(payload + b'\r\n')
    response_dictionary = make_response_dictionary(content_length, path)
    print(response_dictionary)


def response_header(path):
    """
    Creates a response header based on if the file is found in the directory

    :param path: Path of the desired resource file
    :return: 200 OK message if the file was found and a 400 Bad Request otherwise
    """

    if get_file_size(path) is not None:
        header = b'HTTP/1.1 200 OK\r\n'
    else:
        header = b'HTTP/1.1 400 Bad Request\r\n'
    return header


def read_payload(path, content_length):
    """
    Reads the data from the html resource that was requested

    :param path: Path of the requested file
    :param content_length: Length of the payload data in the requested file
    :return: The payload data in bytes
    """

    payload = open(path, 'rb').read(content_length)
    return payload


def make_response_dictionary(content_length, path):
    """
    Makes the response dictionary. If the file doesnt exist, it returns an empty
    dictionary
    :param content_length: Length of the payload data in the requested file
    :param path: Path of the requested file
    :return: The response dictionary
    """

    response_dictionary = {}
    if get_file_size(path) is not None:
        time_stamp = datetime.datetime.utcnow()
        time_string = time_stamp.strftime('%a, %d %b %Y %H:%M:%S GMT')
        response_dictionary = {'Date: ': time_string,
                               'Connection': 'Closed',
                               'Content-Length:': content_length,
                               'Content-Type:': get_mime_type(path)}
        return response_dictionary
    else:
        return response_dictionary


def next_byte(data_socket):
    """
    Read the next byte from the socket data_socket.

    Read the next byte from the sender, received over the network.
    If the byte has not yet arrived, this method blocks (waits)
      until the byte arrives.
    If the sender is done sending and is waiting for your response, this method blocks indefinitely.

    :param data_socket: The socket to read from. The data_socket argument should be an open tcp
                        data connection (either a client socket or a server data socket), not a tcp
                        server's listening socket.
    :return: the next byte, as a bytes object with a single byte in it
    """

    return data_socket.recv(1)


def next_line(data_socket):
    """
     - Reads the next line of the message of the data socket and writes it to the file

     :param data_socket: data_socket to fetch data from
     :return bytes b: The line of bytes read
     :author Jeromy Schultz

     """
    b = b''
    while b[-2:] != b'\r\n':
        b += next_byte(data_socket)
    return b


# ** Do not modify code below this line.  You should add additional helper methods above this line.

# Utility functions
# You may use these functions to simplify your code.


def get_mime_type(file_path):
    """
    Try to guess the MIME type of a file (resource), given its path (primarily its file extension)

    :param file_path: string containing path to (resource) file, such as './abc.html'
    :return: If successful in guessing the MIME type, a string representing the content type, such as 'text/html'
             Otherwise, None
    :rtype: int or None
    """

    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


def get_file_size(file_path):
    """
    Try to get the size of a file (resource) as number of bytes, given its path

    :param file_path: string containing path to (resource) file, such as './abc.html'
    :return: If file_path designates a normal file, an integer value representing the the file size in bytes
             Otherwise (no such file, or path is not a file), None
    :rtype: int or None
    """

    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()

# Replace this line with your comments on the lab

