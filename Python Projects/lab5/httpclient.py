"""
- CS2911 - 011
- Fall 2019

- CS2911 - 011
- Fall 2019
- Lab 5
- Names:
  - Jeromy Schultz
  - Jace Olson

A simple HTTP client
"""

# import the "socket" module -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# import the "regular expressions" module
import re

status_code = ""
have_length = False
have_code = False
chunk = True
length = 4000


def main():
    """
    Tests the client on a variety of resources
    """
    # These resource request should result in "Content-Length" data transfer
    get_http_resource('http://msoe.us/CS/', 'cs1.html')

    # this resource request should result in "chunked" data transfer
    get_http_resource('http://cdn.mathjax.org/mathjax/latest/MathJax.js', 'index.html')

    # If you find fun examples of chunked or Content-Length pages, please share them with us!


def get_http_resource(url, file_name):
    """
    Get an HTTP resource from a server
           Parse the URL and call function to actually make the request.

    :param url: full URL of the resource to get
    :param file_name: name of file in which to store the retrieved resource

    (do not modify this function)
    """

    # Parse the URL into its component parts using a regular expression.
    url_match = re.search('http://([^/:]*)(:\d*)?(/.*)', url)
    url_match_groups = url_match.groups() if url_match else []
    print('url_match_groups=',url_match_groups)
    if len(url_match_groups) == 3:
        host_name = url_match_groups[0]
        host_port = int(url_match_groups[1][1:]) if url_match_groups[1] else 80
        host_resource = url_match_groups[2]
        print('host name = {0}, port = {1}, resource = {2}'.format(host_name, host_port, host_resource))
        status_string = make_http_request(host_name.encode(), host_port, host_resource.encode(), file_name)
        print('get_http_resource: URL="{0}", status="{1}"'.format(url, status_string))
    else:
        print('get_http_resource: URL parse failed, request not sent')


def make_http_request(host, port, resource, file_name):
    """
    - Makes an HTTP request

    :param bytes host: the ASCII domain name or IP address of the server machine (i.e., host) to connect to
    :param int port: port number to connect to on server host
    :param bytes resource: the ASCII path/name of resource to get. This is everything in the URL after the domain name,
           including the first /.
    :param file_name: string (str) containing name of file in which to store the retrieved resource
    :return: the status code
    :rtype: int
    """
    global chunk
    global have_length
    global have_code
    chunk = True
    have_length = False
    have_code = False
    tcp_send(host, port, resource, file_name)
    return status_code


def next_byte(data_socket):
    """
    Read the next byte from the socket data_socket.

    Read the next byte from the sender, received over the network.
    If the byte has not yet arrived, this method blocks (waits)
      until the byte arrives.
    If the sender is done sending and is waiting for your response, this method blocks indefinitely.

    :param data_socket: The socket to read from. The data_socket argument should be an open tcp
                        data connection (either a client socket or a server data socket), not a tcp
                        server's listening socket.
    :return: the next byte, as a bytes object with a single byte in it
    """

    return data_socket.recv(1)


def write(byte, file_name):
    """
    Appends bytes to a .txt file, makes the file if it doesn't exist

    :param byte: byte or bytes to be written to the file
    :param file_name: Name of the file to be written
    :param byte: all bytes read in the file

    :author: Jeromy Schultz
    """

    file = open(file_name, 'ab')
    file.write(byte)
    file.close()


def tcp_send(server_host, server_port, resource, file_name):
    """
    - Sends a http request message to a TCP server

    :param file_name: Name of the file to be written
    :param bytes server_host: name of the server host machine
    :param int server_port: port number on server to send to
    :param bytes resource: The url of the http message
    """

    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_socket.connect((server_host, server_port))
    get_request = b'GET ' + resource + b' HTTP/1.1\r\n' + b'Host: ' + server_host + b'\r\n\r\n'
    tcp_socket.sendall(get_request)
    parse_header(tcp_socket, file_name)
    parse_body(tcp_socket, file_name)
    tcp_socket.close()


def get_status_code(byte_line):
    """
     - This will change the global variable status_code to the status code found in the message header

     :param bytes byte_line: A line of bytes to be parsed
     """

    global status_code
    status_code = int(byte_line.decode()[9:12])


def get_length(byte_line):
    """
     - If a message contains a content length, this will return that integer

     :param bytes byte_line: A line of bytes to be parsed
     """

    global length
    byte_string = byte_line.decode()
    white_space_index = byte_string.rfind(' ')
    end_line_index = byte_string.rfind('\r')
    length = byte_line[white_space_index + 1: end_line_index]
    return int(length)


def check_for_code(byte_line):
    """
     - Checks a line of bytes if it contains the status code

     :param bytes byte_line: A line of bytes to be parsed
     """

    global have_code
    if b'HTTP' in byte_line and not have_code:
        get_status_code(byte_line)
        have_code = True


def check_for_length(byte_line):
    """
     - Checks a line of bytes if it contains the content length for the message body

     :param bytes byte_line: A line of bytes to be parsed
     """

    global have_length
    global length
    global chunk
    if b'Content-Length: ' in byte_line and not have_length:
        length = get_length(byte_line)
        have_length = True
        chunk = False


def parse_header(data_socket, file_name):
    """
     - Parses through the header of the http response message line by line
     - Checks each line for the status code and the content length if it exists

     :param file_name: Name of the file to be written
     :param data_socket: data socket to fetch data from
     """

    b = b''
    run = True
    while run:
        byte_line = next_line(data_socket, file_name)
        b += byte_line
        check_for_code(byte_line)
        check_for_length(byte_line)
        if byte_line == b'\r\n':
            run = False


def parse_body_length(data_socket, file_name):
    """
     - Parses through the body of the message a number of times equal to the global length variable
     - Writes each byte of the body to the file

     :param file_name: Name of the file to be written
     :param data_socket: data socket to fetch data from
     """

    counter = 0
    while counter < length:
        b = next_byte(data_socket)
        write(b, file_name)
        counter += 1


def next_line(data_socket, file_name):

    """
     - Reads the next line of the message of the data socket and writes it to the file

     :param file_name: Name of the file to be written
     :param data_socket: data_socket to fetch data from
     :return bytes b: The line of bytes read

     """
    b = b''
    while b[-2:] != b'\r\n':
        b += next_byte(data_socket)
    write(b, file_name)
    return b


def get_chunk_length(byte_line):
    """
     - Receives a line of bytes, parses it, and returns the decimal number size of the chunk to be read.

     :param bytes byte_line: A line of bytes that describes the length of the following chunk. Ex: ab7\r\n
     :return length: The length of the chunk to be read

     """
    global length
    byte_string = byte_line.decode()
    end_line_index = byte_string.rfind('\r')
    length_str = byte_string[: end_line_index]
    length = int(length_str, 16)
    return length


def parse_body_chunk(data_socket, file_name):
    """
     - Parses the body if the method used is chunking
     - Reads each line from the body and fetches the length of the chunked message
     - Uses the content-length method to read each chunk

     :param file_name: Name of the file to be written
     :param data_socket: data socket to fetch data from
     :author Jeromy Schultz
     """

    global length
    run = True
    while run:
        byte_line = next_line(data_socket, file_name)
        if byte_line == b'\r\n':
            byte_line = next_line(data_socket, file_name)
        length = get_chunk_length(byte_line)
        if length == 0:
            run = False
        else:
            parse_body_length(data_socket, file_name)


def parse_body(data_socket, file_name):
    """
     - Decides which parsing method to use: chunked or by content-length

     :param file_name: Name of the file to be written
     :param data_socket: data socket to fetch data from
     :author Jeromy Schultz
    """

    if chunk:
        parse_body_chunk(data_socket, file_name)
    else:
        parse_body_length(data_socket, file_name)


main()

