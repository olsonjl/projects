def next_byte():
    """
    Read the next byte from the sender.
    If the byte is not yet available,
    this method blocks (waits) until the
    byte becomes available. If there are
    no more bytes, this method blocks
    indefinitely.

    :return: the next byte, as a bytes
        object with a single byte in it
    """
    def read_message():
