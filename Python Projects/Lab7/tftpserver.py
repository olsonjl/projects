"""
- CS2911 - 011
- Fall 2019
- Lab 7
- Names:
  -

A Trivial File Transfer Protocol Server
"""

import math
import os
# import modules -- not using "from socket import *" in order to selectively use items with "socket." prefix
import socket

# Helpful constants used by TFTP
TFTP_PORT = 69
TFTP_BLOCK_SIZE = 512
MAX_UDP_PACKET_SIZE = 65536


def main():
    """
    Processes a single TFTP request
    """

    client_socket = socket_setup()
    print("Server is ready to receive a request")

    ####################################################
    # Your code starts here                            #
    #   Be sure to design and implement additional     #
    #   functions as needed                            #
    ####################################################
    information = receive_first_msg(client_socket)
    file = read_first_msg(information[0])
    send_file(file, information[1], client_socket)

    ####################################################
    # Your code ends here                              #
    ####################################################

    client_socket.close()


def get_file_block_count(filename):
    """
    Determines the number of TFTP blocks for the given file
    :param filename: THe name of the file
    :return: The number of TFTP blocks for the file or -1 if the file does not exist
    """
    try:
        # Use the OS call to get the file size
        #   This function throws an exception if the file doesn't exist
        file_size = os.stat(filename).st_size
        return math.ceil(file_size / TFTP_BLOCK_SIZE)
    except:
        return -1


def get_file_block(filename, block_number):
    """
    Get the file block data for the given file and block number
    :param filename: The name of the file to read
    :param block_number: The block number (1 based)
    :return: The data contents (as a bytes object) of the file block
    """
    file = open(filename, 'rb')
    block_byte_offset = (block_number - 1) * TFTP_BLOCK_SIZE
    file.seek(block_byte_offset)
    block_data = file.read(TFTP_BLOCK_SIZE)
    file.close()
    return block_data


def put_file_block(filename, block_data, block_number):
    """
    Writes a block of data to the given file
    :param filename: The name of the file to save the block to
    :param block_data: The bytes object containing the block data
    :param block_number: The block number (1 based)
    :return: Nothing
    """
    file = open(filename, 'wb')
    block_byte_offset = (block_number - 1) * TFTP_BLOCK_SIZE
    file.seek(block_byte_offset)
    file.write(block_data)
    file.close()


def socket_setup():
    """
    Sets up a UDP socket to listen on the TFTP port
    :return: The created socket
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('', TFTP_PORT))
    return s


def receive_first_msg(udp_socket):
    info = udp_socket.recvfrom(MAX_UDP_PACKET_SIZE)
    return info


def read_first_msg(msg):
    if is_read(msg[:2]):
        return parse_get(msg[2:])
    else:
        print("Error reading the message")
        return 0


def receive_msg(udp_socket):
    msg = udp_socket.recv(MAX_UDP_PACKET_SIZE)
    if is_ack(msg[:2]):
        return parse_ack(msg[2:])
    if is_error(msg[:2]):
        parse_error(msg[2:])
    else:
        print("Error reading the response message")
        return 0


def is_read(msg):
    if msg == b'\x00\x01':
        return True
    return False


def is_ack(msg):
    if msg == b'\x00\x04':
        return True
    return False


def is_error(msg):
    if msg == b'\x00\x05':
        return True
    return False


def parse_get(msg):
    first_index = msg.index(b'\x00')
    return msg[0:first_index].decode()


def parse_ack(msg):
    return int.from_bytes(msg, 'big')


def parse_error(msg):
    print("Error! Code: " + str(int.from_bytes(msg[:2], 'big')) + ' - ' + msg[2:].decode())
    return 0


def send_file(filename, info, socket):
    num_blocks = get_file_block_count(filename)
    if num_blocks == -1:
        return send_404(info, socket)
    current_block = 1
    while current_block < num_blocks:
        send_block(filename, current_block, info, socket)
        current_block = receive_msg(socket) + 1
        if current_block == 0:
            break
    send_last_block(current_block, filename, info, socket)


def send_block(file, block, info, socket):
    data = b'\x00\x03'
    data += block.to_bytes(2, 'big')
    data = data + get_file_block(file, block)
    socket.sendto(data, info)


def send_last_block(block, filename, info, socket):
    data = b'\x00\x03' + block.to_bytes(2, 'big')
    data += get_file_block(filename, block)
    socket.sendto(data, info)
    if data.__len__() == 512:
        block += 1
        empty_data = b'\x00\x03' + block.to_bytes(2, 'big')
        socket.sendto(empty_data, info)


def send_404(info, socket):
    data = b'\x00\x05\x00\x01File not found\x00'
    socket.sendto(data, info)


main()

